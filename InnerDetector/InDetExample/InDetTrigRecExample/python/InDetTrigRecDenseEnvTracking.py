# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

""" Dense Enviroment  versions of the tracking algorithms
"""

__author__ = "J. Masik and S.Lawlor"
__version__= "$Revision: 1.1 $"
__doc__    = "InDetTrigRecDenseEnvTracking.py"



from InDetTrigRecExample.InDetTrigConfigRecNewTracking import TrigAmbiguitySolver_EF
from InDetTrigRecExample.InDetTrigConfigRecPreProcessing import PixelClustering_EF

class TrigAmbiguitySolverdenseEnv_EF(TrigAmbiguitySolver_EF):
  def __init__(self, name="TrigAmbiguitySolver_Electron_Dense_EF", type=""):
    TrigAmbiguitySolver_EF.__init__(self, name, type, denseEnv=True)

class PixelClusteringdenseEnv_EF(PixelClustering_EF):
  def __init__(self, name="PixelClustering_Electron_EF", type="electron"):
    PixelClustering_EF.__init__(self, name, type, denseEnv=True )

      


