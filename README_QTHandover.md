All of the changes exist in the two directories:

InnerDetector/ 
Trigger/

The basic idea of the project is to implement the Neural network implmeneted in the offline into the online

The tools made for this are to be used in a "dense" tag that when call uses these tools
Offline:
/athena/InnerDetector/InDetExample/InDetRecExample/
Online:
/athena/InnerDetector/InDetExample/InDetTrigRecExample/python/

The main files where I implemented these changes were the following and if you search 'dense' you should fine them:

InDetTrigConfigRecPreProcessing.py
InDetTrigConfigRecLoadTools.py
InDetTrigConfigRecNewTracking.py
InDetTrigRecDenseEnvTracking.py

Note I see that since I worked on this there have been updates to this folder so here I uploaded my whole /athena/InnerDetector/InDetExample/InDetTrigRecExample/python/, so you may need to check and go through the most recent athena release to check if anything I did needs changing to be up to date. The InDetTrigRecDenseEnvTracking.py takes the chain I made and allows in to be called in the following Trigger directories:
///

/athena/Trigger/TrigTools/TrigInDetConf/python/TrigInDetSequence.py
/athena/Trigger/TriggerCommon/TriggerMenu/python/bjet/generateBjetChainDefs.py

TrigInDetSequence.py imports InDetTrigRecDenseEnvTracking.py and allows 'dense' to be called at the relevant part of the trigger chain. It would be good to go back through this logic/

generateBjetChainDefs.py allows the dense to be incorparated into the generation of the Bjet chain definitions

This isn't atutomated(apologies I couldn't get this to work). So below this would run with "dendeEnv" on. Basically it is added/removed anywhere in this file where IDTrig occurs in these chaind to turn on/off

On:

    #Use below block for dense on
    if 'FTKVtx' in FTKFlavour:
        [trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig", sequenceFlavour=["denseEnv"]).getSequence() # new Sean has sequence flavour denseEnv etc , sequenceFlavour=["denseEnv"]
        [ftkvtx, trkftk] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx"]).getSequence() # new
    elif 'FTKRefit' in FTKFlavour:
        [ftkvtx, trkftf, trkprec] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx","refit","PT"]).getSequence() # new
    elif 'FTK' in FTKFlavour:
        [ftkvtx, trkftf, trkprec] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx","PT"]).getSequence() # new
    elif useTRT :
        [trkvtx, trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig", sequenceFlavour=["2step","denseEnv"]).getSequence() # new sean had denseenv , "denseEnv"
    else :
        [trkvtx, trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig", sequenceFlavour=["2step","noTRT"]).getSequence() # new

Off:

    #Use below block for dense on
    if 'FTKVtx' in FTKFlavour:
        [trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig").getSequence() # 
        [ftkvtx, trkftk] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx"]).getSequence() # new
    elif 'FTKRefit' in FTKFlavour:
        [ftkvtx, trkftf, trkprec] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx","refit","PT"]).getSequence() # new
    elif 'FTK' in FTKFlavour:
        [ftkvtx, trkftf, trkprec] = TrigInDetFTKSequence("Bjet", "bjet", sequenceFlavour=["FTKVtx","PT"]).getSequence() # new
    elif useTRT :
        [trkvtx, trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig", sequenceFlavour=["2step"]).getSequence() # 
    else :
        [trkvtx, trkftf, trkprec] = TrigInDetSequence("Bjet", "bjet", "IDTrig", sequenceFlavour=["2step","noTRT"]).getSequence() # new

To setup the project I do a sparese checkout, 'mkdir' the build directotry at the same directory level as the athena folder and then 'make' the 'build' folder from within the 'Projects' folder inside the athena folder.

I then make another directory at he same level as athena and build called output that i run everything in remmeber before running/or after edits of code, to implment them you need to source. So I do something like:


setupATLAS
asetup Athena,21.3,latest,slc6
source ../build/x86_64-slc6-gcc49-opt/setup.sh

To run I could do something like:

athena.py  -c ‘eventMax=500;runMergedChain=True;globalTag="OFLCOND-RUN12-SDR-17"' TrigInDetValidation_RTT_topOptions_BjetSlice.py